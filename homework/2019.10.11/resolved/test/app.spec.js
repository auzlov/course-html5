// по одному describe на файл, по сути должно быть 4 файла
// можно не писать слово function, достаточно имени
// в js принято использовать '', "" крайне редко, ещё есть `` если внутри планируется использовать переменную
describe("yesOrNo function", function () {
    it("should say yes if true", function () {
        expect(yesOrNo(true)).toBe("Yes");
    });

    it("should say no if false", function () {
        expect(yesOrNo(false)).toBe("No");
    });

    it("should be null for incorrect data", function () {
        expect(yesOrNo(146)).toBeNull();
    });
});

describe("counter function", function () {
    // никаких циклов внутри тестов. Достаточно проверить пару значений руками 1, 5, 9, 10, 100, null
    it("should return number if number from 1 to 9", function () {
        for (let number = 1; number <= 9; number += 1) { // number++
            expect(counter(number)).toBe(Number(number)); // зачем здесь Number?
        }
    });

    // неверно, в одном it должен быть один expect
    it("should return 9+ if number greater than 9", function () {
        const expected = "9+";
        expect(counter(9.1)).toBe(expected);
        expect(counter(10)).toBe(expected);
        expect(counter(+Infinity)).toBe(expected);
    });

    it("should be null for incorrect data", function () {
        expect(yesOrNo(undefined)).toBeNull();
    });
});

describe("calculateAge function", function () {
    it("should return positive age as full years", function () {
        const now = new Date();
        const thisYear = now.getFullYear();
        const thisMonth = now.getUTCMonth();
        const thisDay = now.getUTCDate();
        // проверь несколько конкретных дат, без циклов
        for (let age = 3; age < 65; age++) {
            let birthYear = thisYear - age;
            let birthDate = Date.UTC(birthYear, thisMonth, thisDay - 1);
            expect(calculateAge(birthDate)).toBe(age);
        }
    });

    it("should be 28 for 12.12.1990", function () {
        expect(calculateAge(new Date('12.12.1990'))).toBe(28);
    });

    it("should be null for date in the future", function () {
        expect(calculateAge(new Date('12.12.2202'))).toBeNull();
    });

    it("should provide correct age for old date", function () {
        expect(calculateAge(new Date('17.02.1476'))).toBe(543);
    });

    it("should provide correct age for date", function () {
        expect(calculateAge(new Date('17.02.1945'))).toBe(74);
    });
});

describe("getRandomItem function", function () {
    // сложно, в тестах не должно быть ни циклов, ни условий
    // создай массив [1,2,3] и проверь что result >=1 && result <=3 toBeTruthy
    it("should return random item from array", function () {
        const maxLength = 1024;
        const length = 1 + Math.floor(Math.random() * maxLength);
        let list = new Array(length);
        for (let index = 0; index < list.length; index++) {
            let random = Math.random();
            if (random < 0.1) {
                list[index] = null;
            } else if (random > 0.9) {
                list[index] = undefined;
            } else {
                list[index] = Math.floor(Math.random() * 1488);
            }
        }

        for (let trying = 0; trying < list.length; trying++) {
            let randomItem = getRandomItem(list);
            expect(list.includes(randomItem)).toBe(true);
        }
    });
});
